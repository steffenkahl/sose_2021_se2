﻿using System;
using TMPro;
using UnityEngine;

namespace _04_Countdown
{
    public class Countdown : MonoBehaviour
    {
        [SerializeField] private float countdownStart;
        [SerializeField] private float countdown;
        [SerializeField] private bool countdownActive;
        
        [SerializeField] private TextMeshProUGUI textMesh;

        private void Start()
        {
            countdown = countdownStart;
        }

        private void Update()
        {
            if (countdownActive)
            {
                if (countdown > 0)
                {
                    countdown -= Time.deltaTime;
                    textMesh.text = ((int) countdown).ToString();
                }
                else
                {
                    textMesh.text = "FINISHED";
                    countdownActive = false;
                }
            }
            else
            {
                if (countdown > 0)
                {
                    textMesh.text = "PAUSED (" + ((int) countdown).ToString() + ")";
                }
                else
                {
                    textMesh.text = "FINISHED";
                }
            }
        }
    }
}