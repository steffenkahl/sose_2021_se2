﻿using UnityEngine;

namespace _03_PlayerSpriteChanger
{
    public class PlayerSpriteChanger : MonoBehaviour
    {
        [SerializeField]
        private SpriteRenderer playerSpriteRenderer;

        public void ChangeToSprite(Sprite newPlayerSprite)
        {
            playerSpriteRenderer.sprite = newPlayerSprite;
        }
    }
}