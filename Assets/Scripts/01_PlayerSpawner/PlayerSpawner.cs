﻿using System;
using UnityEngine;

namespace _01_PlayerSpawner
{
    public class PlayerSpawner : MonoBehaviour
    {
        [SerializeField] private GameObject playerPrefab;

        private GameObject spawnedPlayer;

        private void Start()
        {
            
        }

        public void SpawnThePlayer()
        {
            if (spawnedPlayer == null)
            {
                spawnedPlayer = Instantiate(playerPrefab);
            }
        }

        public void DestroyThePlayer()
        {
            if (spawnedPlayer != null)
            {
                Destroy(spawnedPlayer);
            }
        }
    }
}