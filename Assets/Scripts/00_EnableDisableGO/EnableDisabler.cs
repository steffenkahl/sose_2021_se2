﻿using System;
using UnityEngine;

namespace _00_EnableDisableGO
{
    public class EnableDisabler : MonoBehaviour
    {
        [SerializeField] private string otherGoName;
        
        [SerializeField] private bool shouldGameObjectBeEnabled;

        private GameObject otherGo;
        
        private void Start()
        {
            otherGo = GameObject.Find(otherGoName); //Player-GameObject
        }

        private void Update()
        {
            otherGo.SetActive(shouldGameObjectBeEnabled);
        }
    }
}