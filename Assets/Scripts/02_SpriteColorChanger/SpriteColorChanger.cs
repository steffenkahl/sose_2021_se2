using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace _02_SpriteColorChanger
{
    public class SpriteColorChanger : MonoBehaviour
    {
        [SerializeField] private int health = 100;

        [SerializeField] private Color deathColor;
        private Color standardColor;

        private SpriteRenderer spriteRenderer;

        private void Awake()
        {
            spriteRenderer = GetComponent<SpriteRenderer>();
            standardColor = spriteRenderer.color;
        }

        private void Update()
        {
            if (health <= 30)
            {
                spriteRenderer.color = deathColor;
            }
            else
            {
                spriteRenderer.color = standardColor;
            }
        }
    }
}
